Dear Michelle,

I just thought I would write a quick email summarising what we covered yesterday. 

The primary motivation for finding overarching statistics is the impact statement.  
 1. The ideal statistic would be the amount of money saved in public services.
    - Shelter have some figures that may help us quantify this.
 2. Another number of interest is the number of people using ssj services.
    - This is the place we must start at if we wish to use existing data. In order to collate any other numbers we must know which data entries belong to the same service user (SU).
 3. Further there is a progress "star", if we could draw averages of these measures, that would be good information.

This task has two angles, improving the system going forwards to make acquiring this data easier and retrieving statistics from the data that is currently held.

I will assist with the first problem. I will begin by seeking to identify duplicate SUs between two datasets.
There are possibly 20 separate datasets.
The datasets held by the housing program are first priority as we suspect that they represent the largest financial movements.
These are in Pyramid.

About Pyramid I have some good news, there is a python interface!
I'm assuming that it's this program; https://www.pyramidanalytics.com/ yes?
This means that any script I write can retrieve data directly, no need to read it out into any other format. 

It looks like ILLY may also allow jQuery, but I'm not certain of this. 
Also I'm new to javascript, so it might take a while to get up to speed.
However, there is a distinct possibility of interfacing directly with both database types.

This changes the game a bit.
It may well be possible for my program to retrieve the data it needs directly, so I don't necessarily need to know what fields are held ahead of time.
If the program is getting the data it will have access to all the fields.

The only thing that you need to organise on your end is the data protection arrangements.
What sort of agreement do we need to have in place so that I can work with this data?
If you want me to work with the data on your machine, I will probably need an administrator account in order to set up a coding environment.
I am also happy to work on my own machine, but I suspect this is is not in line with data protection as my machine is unencrypted.
