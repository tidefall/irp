\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Literature review}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Deep neural nets}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Structure of a deep neural net}{3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Single neuron}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Training the single neuron}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}A statistical or matrix view}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.4}Neurons combined as a network}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.5}Activation functions}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Training a deep neural net}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Regularization}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Methodology}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Data set}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Preprocessing}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Splitting by vertex type}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Set of variables}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Reweighing jet \(\eta \) and jet \(p_\text {T}\)}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}CSVv2 imitation}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Minimal approach}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Results}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Target flavour balance}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Reweighing jet \(\eta \) and jet \(p_\text {T}\)}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}CSVv2 imitation}{27}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Minimal approach}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusions}{35}
