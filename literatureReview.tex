
\section{Literature review}
In order to provide some background in deep learning I will provide a literature review that looks at the fundamentals of neural networks.
This should be accessible to someone with no prior experience of machine learning.
It aims to bring a physicist up to speed with the principles behind the tools that will be used later in this work.

This review is about getting a sense of the state of play, rather than answering a specific question. Its topic is quite broad.
Due to the limits on the time available I will not be able to review every, or even the majority of, the literature available in this review.
This will clearly lead to a narrative literature review as opposed to a systematic one.

\subsection{Deep neural nets}
Neural nets and deep neural nets provide tools for solving a class of statistical problems.
As the interest in these tools is practical, clarity is more desirable than precision here. This section is drawn from texts recommended to me by Professor Srinandan Dasmahapatra, who specialises and teaches in the field of  machine learning. 

It is a matter of debate whether a neural net work is supposed to actually resemble a biological network on neurons \cite[p. 470]{mackay_information_2015} or simply has some symbolic parallels \cite[p. 13]{goodfellow_deep_2016} or rather more scathingly that neural nets are the ``subject of exaggerated claims regarding their biological plausibility."\cite[p. 266]{bishop_pattern_2006}. The particular kind of neural network to be used is a multilayer perception, or Feedforward neural network. It seems that in computer science the name Feedforward neural network is preferred  \cite{bishop_pattern_2006}\cite{goodfellow_deep_2016}\cite{murphy_machine_2012}, although there are some exceptions \cite{mackay_information_2015}. The term multilayer perception is seen a misleading because a perception is normally considered to have a discreet output, but the neurons in the model are continuous\cite[p. 226]{bishop_pattern_2006}. There are many more terms used to describe this machine, deep directed network for example \cite{murphy_machine_2012}, some of them have somewhat distinct meanings, but the machine described is broadly similar. The system is a series of regressions models, and it can be used for classification or regression.

Describing a neural net as `deep' indicates that it has some number of hidden layers \cite{goodfellow_deep_2016}\cite{murphy_machine_2012}. The origin of the term `deep' is not too well documented. Goodfellow provides two inconsistent explanations; `deep' refers to the many conceptual layers involved in identifying objects \cite[p. 1]{goodfellow_deep_2016} or alternatively `deep' refers to the number of steps through the many layers of the model \cite[p. 169]{goodfellow_deep_2016}.

\subsection{Structure of a deep neural net}

The aim of a neural network is to learn a relationship between some input vector and an output vector \cite{mackay_information_2015}. 
There are three common approaches to describing the structure of a neural network: a biological approach, as in \cite{mackay_information_2015}; drawing from the language of statistics as in  \cite{bishop_pattern_2006}; and closely related to the statistical approach it can be described as a set of vector-matrix operations, \cite{murphy_machine_2012}, \cite{goodfellow_deep_2016}. 

\subsubsection{Single neuron}
In a biological description the building block is the neuron. When viewed in a snapshot a neuron is a container that takes many inputs, \(x_i\) where \(i = 1,\dots, I\), each input being a single number describing the data. These inputs are multiplied by weights, \(w_i\). Finally it takes in a bias, sometimes written \(x_0\) or \(b\), a single number that is not directly dependent on the data. Note that this bias is of no relation to a bias in statistics. All the weighted inputs and the bias are summed, the result of this sum is entered into an activation function, \(y()\), the output of which is a single number. The output of the activation function is what is returned by the neuron, this is called the activity of the neuron. This description is expressed as a diagram in figure \ref{fig:single_neuron}. \cite{mackay_information_2015}

This notation for the weights, \(w\), and inputs, \(x\), each with appropriate subscripts, is very widespread it can be found in \cite{mackay_information_2015}, \cite{barber_bayesian_2017}, \cite{bishop_pattern_2006} and only slightly modified in \cite{murphy_machine_2012} and \cite{goodfellow_deep_2016}. The notation used for bias is mostly \(w_0\), or sometimes \(b\), with possible superscripts. This notation is much more variable, however, it is not too much trouble as the bias can be seen as equivalent to providing every neuron with an additional \(x_n\) input that is clamped to 1 \cite[p. 570]{murphy_machine_2012}, and so is easy to retroactively include in any mathematical context. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{single_neuron}
    \caption{A neuron with 4 inputs coming in from the left, \(x_1\), \(x_2\), \(x_3\), \(x_4\). With weights \(w_1\), \(w_2\), \(w_3\), \(w_4\). It takes the weighted sum of the inputs then applies its activation function, \(y\). The diagram then shows this output being fed to 4 more unknown objects off to the right. }
    \label{fig:single_neuron}
\end{figure} 

Without knowing the activation function the it is already possible to say a fair bit about the output of this perception.
Slipping briefly into vector notation, if I take the weights as components of a vector, \(\vec{w}\) ``on any line perpendicular to \(\vec{w}\), the output is constant; and along a line in the direction of \(\vec{w}\), the output [will change as the activation function]" \cite{mackay_information_2015}.
If the activation function were a Heaviside function the neuron would be doing binary classification by dividing the input space, \(x_i\), with a plane. This Heaviside function neuron, that produces a binary output, is sometimes referred to as a perceptron \cite{bishop_pattern_2006}. 

\subsubsection{Training the single neuron}\label{sec:TrainingSingle}
It is worth considering how this neuron can learn in isolation as it gives some insight into what might be expected of a group of neurons \cite[p. 475]{mackay_information_2015}. I will now break out of the `snapshot' and look at how the neuron changes with training. This will require some training data, say \(M\) examples, with \(M\) matching target outputs. If my neuron has \(N\) inputs then each training example consists of \(N\) numbers, \(x_1 \dots x_N\) and each target is a single number. These training examples approximate the ideal behaviour of the neuron, if the numbers of the \(m\)th input, \(\vec{x}_m\), are fed to the neuron I would like something like the \(m\) target, \(y_m\), to come out. The parameters that can be altered are the weights, \(w_1 \dots w_N\), and the bias, \(w_0\).  To chose the best weights and bias I must have some way of measuring how good each choice is, this measure is know as the loss function \cite{murphy_machine_2012}, error function \cite{mackay_information_2015}, (negative) objective function \cite{barber_bayesian_2017} interchangeably. This is the function that the neuron will be optimised on. 
In a classification problem the core of this function is normally a term that performs maximum likelihood estimation\footnote{It may also include a regulisation term, I discuss this in a moment.}. That is, for a given set of training data the likelihood of that training data being correctly classified under the net will be maximised. This is conveniently done by minimising the negative log likelihood, know as the cross entropy\cite[p. 132]{goodfellow_deep_2016}. 

To obtain the form of this term start with the negative log likelihood for a set of independent identically distributed inputs; \cite[p. 128]{murphy_machine_2012}
\[\text{NLL}(w_n) = -\sum_{m=1}^M \log{p(y_m|(x_n)_m, w_n)}\]
If the network is doing binary classification then the output of the network will somehow be interpreted as a probability between \(0\) and \(1\). This is a guess as to how likely it is that the input given belongs to the class being classified. 
I will call the guess made by the neural-network from the \(m\)th training input \(\hat{y}_m\), in which case the likelihood of a single training example will be
\[p(y_m|(x_n)_m, w_n) = \hat{y}_{m}^{y_m}(1-\hat{y}_m)^{1-y_m}\]
    Putting these things together leads to
    \[\text{NLL}(w_n) = -\sum_{m=1}^M y_m\log{(\hat{y}_{m})} + (1-y_m)\log{(1-\hat{y}_m)}\]

The values of \(\hat{y}_m\) are dependent only on the inputs, \((x_n)_m\), and the weights and biases of the network.

Now imagine a regression instead of a classification. The aim is to get the network to match target data drawn from a continuous distribution. There is some noise in the distribution, and so there may be more that one possible training target for identical training inputs. Ideally the neural network would guess the mean value of the distribution that the training targets have been drawn from. 

If that distribution is a Gaussian distribution then the likelihood of a single training example is
\[p(y_m|(x_n)_m, w_n) = \frac{1}{\sqrt{2\pi}\sigma}\exp{\left(\frac{-(y_m - \hat{y}_m)^2}{2\sigma^2}\right)}\]

And so the negative log likelihood that will be minimised is
\[\text{NLL}(w_n) = -\frac{1}{2}M\log{(2\pi\sigma^2)} - \sum_{m=1}^M\frac{(y_m - \hat{y}_m)^2}{2\sigma^2}\]

Which is just minimising the mean squared error. To work back the other way, it is reasonable to minimise the mean squared error of the neural net if the noise in the training data is plausibly Gaussian. \cite[p. 134]{goodfellow_deep_2016}\cite[p. 218]{murphy_machine_2012}


Whatever the form of the term the key point is that it is an analytic relation between the weights and biases and the measure of the performance of the neural net on a particular sample. The local gradient with respect to performance is possible to obtain analytically.
Each weight (and the bias) to the neuron will have it's own local gradient.
This gradient is found using a partial derivative with respect to that weight or bias.
This is the simplest possible example of what is known as backpropagation. \cite[p. 475]{mackay_information_2015} 

Using the gradients found by backpropagation to improve the neuron is simple.
The local gradient of the loss function for a particular training example is used to slightly alter the weights and bias of the neuron to according to the local gradient found for that example. Each ``epoch" is the repetition of this for each example in the training set. \cite{murphy_machine_2012}\cite{goodfellow_deep_2016}\cite{barber_bayesian_2017} The whole training process may require a number of epochs. Now the neuron should be performing better on the training data, however, it may still not generalise now if the optimisation has caused over fitting. 

In what way can a neuron overfit? Say the neuron is being used for classification, and so its activation function is a sigmoid, as shown in figure \ref{fig:activation}. The inputs form a \(N\) dimensional space in which the neuron produces a gradient from \(0\) to \(1\). The direction of this gradient is controlled by the ratio of the weights, the displacement from the origin is controlled by the bias and the steepness of this gradient is controlled by the magnitude of the weights. If the training targets are binary (each example is either in group 1 or group 0) then the optimisation process will align the gradient in the way that best separates the training data. If the data does happen to be linearly separable then the optimiser will keep making the gradient steeper so that the sigmoid tends towards a Heaviside function.
This is undesirable because it will classify points in a way that assumes that the training data provides enough information to make a decision with certainty about any further points. This is of course an example of over fitting. This is illustrated in figure \ref{fig:overfitting}. If the activation function where something else then an increase in the magnitude of the weights would squash it along the direction of the gradient. This would also cause over fitting, so the desire to keep the magnitude of the weight small is not unique to sigmoid activation functions. \cite[p. 477]{mackay_information_2015} 

Although not mentioned in \cite{mackay_information_2015} in many physics applications this does not pose to be a problem because there are normally enough training examples, and enough noise in the examples, that they are not linearly separable.
The noise it self acts as a regularizer.

\begin{figure}[!htb]
    \minipage{0.32\textwidth}
    \includegraphics[width=\linewidth]{underFit.pdf}
    \caption*{This neuron is under fitting.}
    \endminipage\hfill
    \minipage{0.32\textwidth}
    \includegraphics[width=\linewidth]{goodFit.pdf}
    \caption*{This neuron has the right fit for the data.}
    \endminipage\hfill
    \minipage{0.32\textwidth}%
    \includegraphics[width=\linewidth]{overFit.pdf}
    \caption*{Over fitting, neuron now resembles a binary classifier.}
    \endminipage
    \caption{A neuron with 2 inputs fitting some data points. The axis are the possible values of the input and the colour is the output of the neuron at that point.}\label{fig:overfitting}
\end{figure}
So how can the optimiser be discouraged from over fitting? The optimiser is optimising for the smallest loss function, so over fitting can be discouraged by adding a regularization term to the loss function that grows with to the magnitude of the weights.
Specifically, a term that is proportional to the squared 2 norm of the weights, \(\alpha ||w_i||^2_2\).
This term is often called a weight decay term \cite{bishop_pattern_2006}\cite{goodfellow_deep_2016}\cite{mackay_information_2015},
although it is also known as \(l^2\) regularization, or Tikhonov regularization\cite[p. 231]{goodfellow_deep_2016}. A common variant on it is called ridge regression\cite[p. 226]{murphy_machine_2012}\cite[p. 363]{barber_bayesian_2017}, in this case the term is simply the magnitude of the weights; \(\alpha ||w_i||_1\).
Goodfellow describes the process of regularization as ``any modification we make to a learning algorithm that is intended to reduce its generalization error but not its training error" \cite[p. 120]{goodfellow_deep_2016}. The weight decay term fits this description well - it will always increase the loss function for the training data, but by preventing over fitting it should lead to a better match on real data. 


It should be noted that this weight decay term is sometimes not combined with the loss directly, but supplied to the optimiser.
In many cases this is mathematically equivalent, simply more computationally efficient, but for the popular `Adam' optimiser is is not quite equivalent.
The Adam optimiser uses the second moments of the gradient, this gives it an advantage in systems that may have a sparse gradient \cite{kingma_adam_2014}.
While weight decay plays the same role here, there are some subtle mathematical differences, for a full discussion see \cite{loshchilov_fixing_2018}.

\subsubsection{A statistical or matrix view} \label{sec:statmat}
 Although Bishop \cite{bishop_pattern_2006} devotes a full chapter to neural networks the word `neuron' is not used once.
 Instead the term used is `units' and they are described as basis functions that perform transformations on the data.
 The construction of the units is compared to regression models. \cite[p. 277]{bishop_pattern_2006}
 
 Another subtly different presentation is given in Murphy\cite{murphy_machine_2012}. Here the components of a neuron, weights, inputs and bias are each notated as vectors.
 The neuron in figure \ref{fig:single_neuron} would represent the formula;
 \[
     z_\text{out} = y(\vec{w}\vec{x} + \vec{b})
 \]
 Which is rather concise.
 In this picture a whole neural network is also easier to put into an equation. 
 Although perhaps this form lacks some of the intuition of the biological approach.


\subsubsection{Neurons combined as a network}
Now that the properties of a single neuron have been examined it is not such a big step to look at the neural network. This is elegantly depicted as rows of neurons feeding off each other, as in figure \ref{fig:neural_net_weight_labels}. The net depicted is fully connected, in other nets some nodes may not pass their values to all nodes in the next layer.
The overall action of this collection of neurons is to take a vector of numbers, the inputs, and return another vector. 
The vector that is return is decided deterministically by weights and biases of the neurons in the network.
The mechanics of this are detailed in the caption of figure \ref{fig:neural_net_weight_labels}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{neural_net_weight_labels}
    \caption{A neural net with two hidden layers. The input layer, on the left in purple, takes three inputs. These inputs are passed to the first hidden layer, in blue, that has four nodes. Each of these four nodes will multiply the input is received by the appropriate weight, possibly add a bias values, and then apply it's activation function to the sum. The process is repeated in the second hidden layer with three nodes. Finally values will be passed to the nodes in the output layer, which will also multiply them by weights and add bias. Often the output layer has a different activation function to the hidden layers. Some, but not all, of the labels for the weights are shown. }
    \label{fig:neural_net_weight_labels}
\end{figure} 
% Say somthing about feed-forward topology
Of course the neurons could be connected together in many other ways that would involve loops, however I am interested in a feed-forward network. ``In a feedforward network, all the connections are directed such that the network forms a directed acyclic graph." \cite[p. 505]{mackay_information_2015}\cite[p. 168]{goodfellow_deep_2016}
These networks are referred to by number of layers, the network depicted in figure \ref{fig:neural_net_weight_labels} is a 3-layer feedforward neural network because ``counting the number of layers of neurons not including the inputs." \cite[p. 527]{mackay_information_2015} it has 3 layers.
Now that there are more parameters to consider a more complex notation is required; 
\begin{itemize}
    \item The weight between node \(j\) of layer \(l-1\) and node \(i\) of layer \(l\) is sometimes written as \(w_{ij}^{(l)}\) when the counting starts from the first layer after the inputs. \cite{mackay_information_2015} \cite{bishop_pattern_2006}\cite{murphy_machine_2012}
        However, caution here as weight at training step \(t\) is in Goodfellow written \( w^{(t)}\) \cite[p. 403]{goodfellow_deep_2016} Goodfellow otherwise prefers bold vector notation.
    \item When bias is still explicitly mention it is sometimes written as \(\theta\) \cite{mackay_information_2015}, however, bias can be absorbed in to the weights by adding an input to each node that is clamped to \(1\)\cite{bishop_pattern_2006}\cite{murphy_machine_2012}
    \item The activation function at layer \(l\) may be written \(f^{(l)}\) \cite{mackay_information_2015}. \(h\) \cite{bishop_pattern_2006} \cite{goodfellow_deep_2016} or simply \(g\) \cite{murphy_machine_2012}.
\end{itemize}

\subsubsection{Activation functions}

If all the activation functions are linear then the network can only do linear regression. \cite[p. 564]{murphy_machine_2012} This is probably not desirable.
On the other hand, if the activation functions are not linear, and there are a sufficient number of nodes in the hidden layer, then the neural network is a universal approximator \cite[p. 564]{murphy_machine_2012} \cite[p. 230]{bishop_pattern_2006}
A universal approximator is a device that can match any function to a chosen degree of accuracy.
This is an exciting property, however, it warrants precautions against overfitting. 

In the early days on neural nets the sigmoid function was common choice of activation function.
Indeed it was so prevalent that many texts still use it as the default choice, although that is now somewhat outdated. \cite{barber_bayesian_2017}\cite{mackay_information_2015}\cite{murphy_machine_2012}\cite{bishop_pattern_2006}

The form of a sigmoid activation function is;
    \[h(x) = \frac{1}{1+\exp{-x}}\]

This is plotted in figure \ref{fig:activation}.
It maps the input function to the range \(0\) to \(1\).
In this respect is follow in the original line of thinking that a neural network is in some way representative of biological neurons.
These can be activate or quiescent, so the sigmoid activation function mimics this with the range \(0\) to \(1\).


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{activation.eps}
    \caption{The sigmoid activation function and the ReLU activation function.}
    \label{fig:activation}
\end{figure} 

% about sigmoid based nns
Unfortunately when the input values are very large in magnitude the gradient becomes vanishingly small.
The process of improving the output of any system via optimisation requires following gradients, so a gradient that may vanish, or saturate, poses a real threat.
For a neural network with internal sigmoid activation functions; 
``saturation of the sigmoid could prevent gradient based learning from making good progress" \cite[p. 183]{goodfellow_deep_2016}.
In fact, in the texts that only describe sigmoid activation functions it is concluded that 
``reliably training these systems is a highly complex task" \cite[p. 377]{barber_bayesian_2017}.

%RelU
Thankfully, there are other stronger choices for activation function in the hidden layer.
A more common choice of activation function now is the ReLU function. \cite[p. 174]{goodfellow_deep_2016}
This is also depicted in figure \ref{fig:activation}. It's piecewise linear form makes the gradient easy to compute, and still allows the network to construct non-linear functions.\cite[p. 175]{goodfellow_deep_2016}

\subsection{Training a deep neural net}

A neural network is trained by an optimiser that utilises the gradients that can be found with the process of back-propagation.
This word is of fairly recent coinage and can be variably rendered as `back-propagation'\cite{goodfellow_deep_2016}\cite{murphy_machine_2012}, `backpropagation'\cite{mackay_information_2015}\cite{murphy_machine_2012}\cite{bishop_pattern_2006} or even just `backprop'\cite{bishop_pattern_2006}.
It is not used at all in some texts.\cite{barber_bayesian_2017}
The back-propagation algorithm was first discovered in 1969, it has been rediscovered a number of times since. \cite[p. 569]{murphy_machine_2012}
This perhaps goes some way to explaining the slight inconsistencies.

Back-propagation is an application of the chain rule to a any graphed function, in our case a neural network, in order to find the dependence of an output on a particular parameter.
For each training example the optimiser needs to know how the error on the gradient behaves for every weight and bias in the system.
Without giving the optimiser a gradient the network would have to remain sparse indeed to avoid the cures of dimensionality.\cite[p. 155]{goodfellow_deep_2016}

The process of backpropagation can be compactly written in matrix form.
Remembering the vector description of a single neuron give in section \ref{sec:statmat};

 \[
     z_\text{out} = y(\vec{s}) = y(\vec{w}\vec{x} + \vec{b})
 \]

In order to discover the gradient of the output, \(z_\text{out}\) with respect to a single weight \(w_i\) the chain rule can be employed.

\[
    \frac{\partial z_\text{out}}{\partial w_i} = \nabla_{\vec{s}}y(\vec{s}) \frac{\partial \vec{s}}{\partial w_i}
\]
The will extend in a trivial way to obtain the gradient with respect to all the weights;
\[
    \nabla_\vec{w} z_\text{out} = \nabla_{\vec{s}}y(\vec{s}) \frac{\partial \vec{s}}{\partial \vec{w}}
\]
Where \(\frac{\partial \vec{s}}{\partial \vec{w}}\) is the Jacobean matrix.
Each layer of the neural network can be traversed with a step like this. \cite[p. 207]{goodfellow_deep_2016}
The equivalent biological description is given in figure \ref{fig:backpropagation}.



\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{back_propagation.pdf}
    \caption{One line of back-propagation on the network depicted in figure \ref{fig:neural_net_weight_labels}. In this example the gradient of the output \(z_1\) with respect to the weight \(w_{34}^{(2)}\) is determined. }
    \label{fig:backpropagation}
\end{figure} 



\subsubsection{Regularization}
In a neural network there are a two factors that effect the complexity of the model, the structure of the network, and the magnitude of the weights.

Increasing the number of neurons in the hidden layers, or increasing the number of hidden layers will increase the capacity of the model \cite[p. 482]{goodfellow_deep_2016}
So one way to control the complexity of the model is to reduce the number of neurons in the hidden layer.
This is the principle behind dropout regularisation. \cite[p. 258]{goodfellow_deep_2016}

Drop out regularisation is effective, and relatively computationally inexpensive. \cite[p. 265]{goodfellow_deep_2016} 

However drop out is not suitable in all cases; ``in the limit as \(H \rightarrow \infty\) the
 statistical properties of the functions generated by randomizing the weights are
 independent of the number of hidden units; so, interestingly, the complexity of
 the functions becomes independent of the number of parameters in the model."\cite[p. 528]{mackay_information_2015}
Thus for larger models another option must be used.

The alternative is a weight decay term, equivalent to the one mentioned in section \ref{sec:TrainingSingle}.
A term is added to the loss function that penalises larger weights.
This is such a common choice for regularisation that the term regularisation is sometimes simply assumed to mean weight decay. \cite[p. 529]{mackay_information_2015}\cite[p. 226]{murphy_machine_2012}

% This brings up a problem with the regularization function that I described earlier, weight decay is not invariant under linear transformations of the data \cite[p.232]{bishop_pattern_2006} \cite[p. 258]{bishop_pattern_2006}. %TODO continue this line of thought
% % Talk about activation function
% ``A key difference compared to the perceptron, however, is that the neural net-
% work uses continuous sigmoidal nonlinearities in the hidden units, whereas the per-
% ceptron uses step-function nonlinearities. This means that the neural network func-
% tion is differentiable with respect to the network parameters, and this property will
% play a central role in network training." \cite{bishop_pattern_2006} but actually ReLU is not continuous, so this is outdated.
% Sigmoid (When is it used?)


