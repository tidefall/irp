\section{Results}

\subsection{Target flavour balance}

The data in the sample contained \(83.0 \%\) b-jets, \(1.8 \%\) c-jets and \(15.2 \%\) udsg-jets.
To start with all data has weight \(1\), then a new data set is created such that the sum of the weights in each target flavour is equal.
the first data set is called `unbalanced', it will lead to a net that sees many b-jets and few c-jets.
The second data set is called `balanced' and the new will see jets of every flavour equally as it trains.

First the progress of the training is plotted in figure \ref{fig:training_balence_tars}.
In the plot of loss over time it can be seen that the `unbalanced' data set makes it easier to achieve a consistently low loss,
this is not helpful to the optimiser.
If the loss is low no matter what then the optimiser cannot improve it very well so it instead turns to simplifying the model by reducing the weights, which can be seen in the lower plot.

By comparison the loss function is much larger for the balanced data, but it is also more mobile, and descends further.
Despite a weight decay term in the optimiser the weights increase overtime because a more complex model if favourable.

\begin{figure}
    \includegraphics[width=1.2\textwidth, center]{target_balence_loss}
    \caption{The training progress of the same net on data that has been balanced by target flavour, so that the net encounters all flavours of jet with the same frequency, compared to data that is unbalanced.
            The weight decay term is in the optimiser, so the loss is a pure measure of predictive ability on the data set.
            The graph of loss function in both the test and train sets show that the loss function is optimised much faster in the first epoch for the unbalanced data.
            The weights of the balanced data quickly become far larger.
            The weight decay in the optimiser dominates the training for the unbalanced set but not for the balanced set.}
    \label{fig:training_balence_tars}
\end{figure}

In figure \ref{fig:roc_balence_tars} the performance of both options is evaluated. The `balanced' dataset created a significant improvement in the networks ability to recognise c-jets at only a small cost to the ability to identify b and udsg-jets.

\begin{figure}
    \includegraphics[width=1.\textwidth, center]{target_balence_roc}
    \caption{The roc curve of the same net on data that has been balanced by target flavour, so that the net encounters all flavours of jet with the same frequency, compared to data that is unbalanced.
    The greatest impact is on the ability of distinguish c jets, in this balancing the targets gives a great advantage because there are few c jets in the data set.}
    \label{fig:roc_balence_tars}
\end{figure}



\subsection{Reweighing jet \(\eta\) and jet \(p_\text{T}\)} \label{sec:reweigh}

The next step to be taken is reweighing to match jet \(\eta\) and jet \(p_\text{T}\) distributions across the target jet flavours.

This is done with the algorithm given in section \ref{sec:methreweigh}.
Reweighing was done with only partial success. 
In order to limit the effect on other distributions no one jet was rescaled to more than \(15\) times it's original weight.
The effect of this can be seen in figure \ref{fig:jet_reweigh_matches}.
The distributions do become considerably closers but do not quite line up.
Figure \ref{fig:problem_distributons} shows the effect that the reweighing had on some other distributions.
It is not drastic, but there are definitely changes visible.

It is also the case that the implementation I created to do this reweighing was very inefficient.
The process took an amount of compute time that was prohibitive to attempting variations.

\begin{figure}
\begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\linewidth]{RecoVertex_jet_pt_}
    \label{fig:jet_pt_reweigh}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\linewidth]{RecoVertex_jet_eta_}
    \label{fig:jet_eta_reweigh}
\end{subfigure}
\caption{A plots of the variables which required matched distributions across the flavours.
    The density is estimated using a kernal density function. The dotted line is the distribution of the variable before the attempted reweighing and the solid line and fill is the variable post reweighing. 
        The algorithm I designed to calculate the new weights is iterative.
        Factors that limited how well the final distributions matched were; a decision not to allow any jet weight to be greater than 15 times the starting base weight, and a desire not to warp the distributions of the other variables too strongly.
    There was some effect on the other variables, see figure \ref{fig:problem_distributons}.
    }
    \label{fig:jet_reweigh_matches}
\end{figure}


\begin{figure}
\begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\linewidth]{RecoVertex_SV_delta_R_}
    \label{fig:SV_delta_R_reweigh}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\linewidth]{RecoVertex_SV_energy_ratio_}
    \label{fig:SV_energy_ratio_reweigh}
\end{subfigure}
\caption{Plots of some of the variables that were adversely effected by the reweighing to match jet \(p_\text{T}\) and jet \(\eta\).
    There was some effect of all variables, but these represent the most significant changes.
    }
    \label{fig:problem_distributons}
\end{figure}

In figure \ref{fig:training_reweigh_vs_raw} the training progress of the nets with an without this reweigh is plotted.
This plot shows that reweighing the data, even as little as has been done, makes a significant difference. 
The reweighed data both trains better and leads to a more complex model.

Note that the data in the testing set is never reweighed, so it is a comparable measure. It is flavour balanced.


\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{loss_reweigh_vs_raw}
    \caption{The training progress of nets of the CSVv2 design trained on data that has been reweighed and raw data. 
        Note both raw data and reweighed data are target flavour balanced.
    }
    \label{fig:training_reweigh_vs_raw}
\end{figure}


The discriminator of each net acting on the data set is plotted in figure \ref{fig:hist_preds_CSVv2_replica}.
This plot is designed to be comparable to figure 12 in \cite{cms_collaboration_identification_2017}, reproduced in this paper as figure \ref{fig:fig12}.


\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{figure12}
    \caption{Figure 12 of \cite{cms_collaboration_identification_2017}, the distribution of the discriminator of the real CSVv2 predicting ``is b-jet".
    }
    \label{fig:fig12}
\end{figure}

\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{reweigh_vs_raw_all_vars_hist_preds}
    \caption{The predictions of nets of CSVv2 design trained on data that has been reweighed and raw data. 
        Both nets are tested on the same data set.
        Note both raw data and reweighed data are target flavour balanced.
    }
    \label{fig:hist_preds_reweigh_vs_raw}
\end{figure}

Receiver operating characteristic (roc) caves for each jet on each flavour are plotted in figure \ref{fig:roc_reweigh_vs_raw}.
From the roc curves it can be seen that this reweighing of the data has significantly improved the networks capacity to identify c-jets.
There is little change in it's performance on b or udsg-jets.

\begin{figure}
    \includegraphics[width=0.9\textwidth,center]{reweigh_vs_raw_all_vars_roc}
    \caption{The efficiency of nets of CSVv2 design trained on data that has been reweighed and raw data. 
        Both nets are tested on the same data set.
        Note both raw data and reweighed data are target flavour balanced.
    }
    \label{fig:roc_reweigh_vs_raw}
\end{figure}

\subsection{CSVv2 imitation}

Now all the groundwork has been done a full run of the neural network that imitates CSVv2 is done.
To offer points of comparison it is shown alongside a network that uses the variables of CSV (written here `CSVv1'), which can be found \cite[p. 18]{cms_collaboration_identification_2017},
and also all of the secondary vertex variables (written here `all SV').

In figure \ref{fig:training_CSVv2_replica} we can see that the full set of CSVv2 variables trains better and forms a more complex model than either of the reduced options, `CSVv1' or `all SV'.
This is perhaps to be expected.

\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{loss_CSVv2_replica}
    \caption{The training progress of replica CSVv2.
        The data was both reweighed and target flavour balanced.
    }
    \label{fig:training_CSVv2_replica}
\end{figure}

The predictions for each flavour using the CSVv2 replica are shown in figure \ref{fig:hist_preds_CSVv2_replica}, again in a form comparable to \ref{fig:fig12}.
A good match can be seen for the ``is b" predictions.
\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{CSVv2_replica_hist_preds}
    \caption{The predictions of the replica CSVv2.
    }
    \label{fig:hist_preds_CSVv2_replica}
\end{figure}

The performance of all the nets compared in this section is plotted as a roc curve in \ref{fig:roc_CSVv2_comp}.
This shows that the full set of variables does confer a distinct advantage in reliably identifying all flavours.

\begin{figure}
%     \begin{subfigure}{0.5\textwidth}
%     \includegraphics[width=\linewidth]{CSVv2_replica_roc}
%     \caption{The roc curve of the replica CSVv2.
%     }
%     \label{fig:roc_CSVv2_replica}
% \end{subfigure}
% \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=\linewidth]{CSVv2_comp_roc}
    \caption{The roc curve of the replica CSVv2 with others for comparison.
    }
    \label{fig:roc_CSVv2_comp}
% \end{subfigure}
\end{figure}

\subsection{Minimal approach}

The sets of minimally correlated variables discussed in section \ref{sec:methmin} are studied.
The set of all SV variables is chosen a benchmark measure as there are 6 SV variables an 6 low correlation variables when using a full list.

The training progress can be seen in figure \ref{fig:training_cor_comp}. 
This shows that none of the minimally correlated sets of variables are training quite as well as the SV variables.
They are also producing simpler models. 
In all cases however, the magnitude of the difference is not so large as in say the difference between training a full CSVv2 and all VS shown in figure \ref{fig:training_CSVv2_replica}.
\begin{figure}
    \includegraphics[width=1.2\textwidth,center]{loss_cor}
    \caption{The training progress of net trained with minimally correlated variables.
        Also a net of the 6 SV variables for comparison.
        The data was both reweighed and target flavour balanced.
    }
    \label{fig:training_cor_comp}
\end{figure}


The performance plots of the minimally correlated variables can be seen in figure \ref{fig:roc_cor_comp}.
again it can be seen that no one minimally correlated set does as well as the SV set.

\begin{figure}
    \includegraphics[width=0.9\textwidth,center]{cor_comp_roc}
    \caption{The training progress of net trained with minimally correlated variables.
        Also a net of the 6 SV variables for comparison.
    }
    \label{fig:roc_cor_comp}
\end{figure}
