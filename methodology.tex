
\section{Methodology}
The initial goal is to train a neural net so that it can distinguish b-jet, a c-jet and light flavoured (``usdg") jets.
It is also of interest to create networks as close in form to those of CSVv2 \cite{collaboration_identification_2013} and DeepCSV \cite{cms_collaboration_identification_2017}. 
These provide good points of comparison.


The training data will be simulated CMS data.
and it is produced such that it contains only the jets we are interested in working with.
The data comes with some processed variables such as impact parameter (IP), transverse momentum relative to jet axis, track decay length, secondary vertex (SV) mass and a others.
These variables can be used to describe the nature of the jet they refer to, and they are what the neural net is trained on.
A more complete description of this data can be seen in section \ref{sec:dataSet}.


To begin with the neural net is fed a minimal number of variables.
The effects of changing and increasing the number of variables used as input for the neural net can then be observed. 
Once the effects of smaller variable sets has been investigated a full imitation of CSVv2 is attempted. 


\subsection{Data set} \label{sec:dataSet}
% Describe the data set...
In order to train and test this dataset I will use simulated data provided to me by Dr. Emmanuel Olaiya (RAL).
The data is designed to simulate what was recorded by the CMS detector in 2016.
At this time the collisions created by the LHC, which are pp collisions, had a centre-of-mass energy of \qty{13}{\tera\electronvolt} and an integrated luminosity of \qty{35.9}{\per\femto\barn}.


A collection of generators are used to create this data, as detailed in \cite[p. 4]{cms_collaboration_identification_2017}.
Here I will provide a short summary.

The hits in the simulation are first clustered into jets with the infrared and collinear safe anti-\(k_T\) algorithm \cite{cacciari_anti_2008}.

This simulation has then been processed to locate tracks.
Various cuts are then applied; ``tracks are required to have \(p_T >\) \qty{1}{\giga\electronvolt},
a \(\chi^2\) value of the trajectory fit normalized to the number of degrees of freedom below 5, and
at least one hit in the pixel layers of the tracker detector." \cite[p. 6]{cms_collaboration_identification_2017}.
These requirements ensure that jet variables derived from the tracks have a good resolution. 
Some of the \(K^0_S\) and \(\Lambda\) hadrons are filtered by requiring the decay length of the track to be less than \qty{5}{\centi\metre}.
Track decay length is defined in section \ref{sec:variables}.

In order to reduce the number of pileup tracks present two more cuts are applied; ``the absolute value of the transverse (longitudinal) IP of the track is required to be smaller than \qty{0.2 (17)}{\centi\metre} and the distance between the
track and the jet axis at their point of closest approach is required to be less than \qty{0.07}{\centi\metre}." \cite[p. 6]{cms_collaboration_identification_2017}.

These track cuts do have an effect on the distribution of variables in the sample of events. 

After choosing the tracks a vertex finding algorithm is used, in this case the inclusive vertex finding algorithm \cite[p. 10]{cms_collaboration_identification_2017}.
This attempts to find vertices within the jets.

Finally it is possible to obtain a set of higher level variables from the data.

The data provided for this work is this set of higher level variables.
Of the variables available 21 will be utilised, 19 of which are inputs for the machine learning tool, one of which is a target variable, and finally one is used to create cuts. 
The variables themselves have some dependence on the nature of the jets.
This is to allow for jets where the reconstruction was less successful.
Because of this I will list and define the variables in section \ref{sec:variables} of the data processing, once the jets have been sorted according to the success of reconstruction.

\subsection{Preprocessing}

To begin preprocessing the data is moved to \lstinline{hdf5} format.
This offered good reading speed and is portable.
Values that have been recorded as \(-9999.0\) to indicate missing data are converted to \lstinline{n.a.} to reduce the potential for errant statistics.   

\subsubsection{Splitting by vertex type}

As mentioned earlier the jets are reconstructed to different extents.
The problem of classifying jets becomes more tractable if they are split into categories that represent roughly the same level of reconstruction.
Three categories are chosen; \cite[p. 15]{cms_collaboration_identification_2017}
\begin{itemize}
    \item \textbf{RecoVertex:} The jet contains one or more SVs.
    \item \textbf{PsudoVertex:} No SVs, but the tracks fit these conditions;
        \begin{enumerate}
            \item At least 2 tracks with a 2D IP significance above 2. 
                `2D' refers to the IP in the transverse plane.
            \item The combined invariant mass of the tracks selected by the first condition is at least \qty{50}{\mega\electronvolt} away from the \(K^0_S\) mass. 
        \end{enumerate}
    \item \textbf{NoVertex:} All jets that don't meet the requirements for either of the first two categories.
\end{itemize}
Once the jets have been split into these three categories they are processed and used separately in all following steps.
No jets of type PseudoVertex have been found in the data sample used.
The steps are nearly identical for all three categories, the distinction being that they each offer a different number of variables to work with.

\subsubsection{Set of variables} \label{sec:variables}

The identification is done per jet, so each jet has a set of input values and one target value associated with it.
I will list the 19 inputs first, which can also be found here \cite[p. 15]{cms_collaboration_identification_2017}, then the target variable, then the final variable.
Once all the possible variables have been identified which ones can be found in each of the jet categories above will be tabulated.

\begin{enumerate}
    \item \label{SV 2D flight dist sig} \textbf{SV 2D flight distance significance:} The SV flight distance is the distance between the primary and secondary vertices. 
        `2D' refers to movement in the transverse plane.
        This variable is recorded per jet and if there are multiple SVs in the jet then the one with the smallest uncertainty in flight distance is used. 
    \item \label{num SVs} \textbf{Number of SVs:} The number of SVs found in the jet.
    \item \label{trk eta rel} \textbf{Track \(\eta_\text{rel}\):} This is the pseudorapidity of the track relative to the jet axis. It is recorded per track and the track with the smallest uncertanty in flight distance is used.
    \item \label{SV mass} \textbf{Corrected SV mass:} This variable is defined per jet and it's definition depends on the jet category.

        In the RecoVertex category it is the corrected mass of the secondary vertex with the smallest uncertainty in flight distance.
        The correction is ``\(\sqrt{M^2_\text{SV} + p^2\sin^2\theta} + p\sin\theta\), where \(M^2_\text{SV}\) is the invariant mass of the tracks associated with the SV, \(p\) is the SV momentum obtained from the tracks associated with it and \(\theta\) is the angle between the secondary vertex momentum and the vector pointing from the primary vertex to the secondary vertex." \cite[p. 10]{cms_collaboration_identification_2017}

        In the PsudoVertex category this variable is the invariant mass obtained from the total summed four momentum vector of all the tracks in the jet.

        In the NoVertex category this variable is not used.
    \item \label{num trks from SV} \textbf{Number of tracks from the SV:} This variable is defined per jet and it's definition depends on the jet category.

        In the RecoVertex category it is the number of tracks associated with with the smallest uncertainty in flight distance.

        In the PsudoVertex category it is the total number of tracks in the jet.

        In the NoVertex category this variable is not used.
    \item \label{SV energy ratio} \textbf{SV energy ratio:} this is the energy of the SV with the smallest uncertainty on its flight distance divided by the combined energy of all the tracks in the jet. 
    \item \label{SV delta R} \textbf{\(\Delta R(\text{SV}, \text{jet})\):} This is a measure of angular separation in \(\eta\) \(\phi\) space. It is defined per jet and it's definition depends on the jet category.

        In the RecoVertex category it is the angular distance between the jet axis and the SV with with the smallest uncertainty in flight distance.

        In the PsudoVertex category it is the angular distance between the jet axis and the summed four momentum of all the tracks in the jet.

        In the NoVertex category this variable is not used.
    \item \label{3D IP sig} \textbf{Track 3D IP significance:} This variable is defined per track. The values of the four tracks with the highest 2D IP significance are used, so this variable provides 4 values as input data.
    \item \label{trk relative pt} \textbf{Track \(p_{\text{T, rel}}\):} This is the track momentum perpendicular to the jet axis, \(p_\text{T}\) relative to the jet axis. The variable is defined per track and the track with the highest 2D IP significance will be chosen as an input value.
    \item \label{trk delta R} \textbf{\(\Delta R(\text{track}, \text{jet})\):} This is a measure of angular separation in \(\eta\) \(\phi\) space. It is the angular separation between the track and the jet axis. The variable is defined per track and the track with the highest 2D IP significance will be chosen as an input value.
    \item \label{trk pt ratio} \textbf{Track \(p_{\text{T, rel}}\):} This is the track momentum perpendicular to the jet axis divided by the magnitude of the track momentum. The variable is defined per track and the track with the highest 2D IP significance will be chosen as an input value.
    \item \label{trk distance} \textbf{Track distance:} This variable is defined per track, it is the minimal distance between the track and the jet axis. The track with the highest 2D IP significance will be chosen as an input value.
    \item \label{trk decay length} \textbf{Track decay length:} This variable is defined per track, it is the distance between the primary vertex and the point of closest approach between the track and the jet axis. Note the somewhat unusual definition. The track with the highest 2D IP significance will be chosen as an input value.
    \item \label{summed trks Et ratio} \textbf{Summed tracks \(E_\text{T}\) ratio:} This variable is defined per jet. It is the summed transverse energy of all tracks in the jet divided by the transverse energy of the jet.
    \item \label{summed delta R} \textbf{\(\Delta R(\text{summed tracks}, \text{jet})\):} This is a measure of angular separation in \(\eta\) \(\phi\) space. It is defined per jet and it is the angular separation between the summed for momentum of the tracks and the jet axis.
    \item \label{first trk 2D IP sig above charm} \textbf{First track 2D IP significance above c threshold:} This variable is defined per jet.
        It is the 2D IP significance of a chosen track.
        The track is chosen by adding the four momenta of the tracks in order of least uncertainty in flight distance %TODO, is that actually the order they are added in? It dosn't say anywhere in the paper. I'm making it up here.
        until the combined four momentum vector has a mass greater than \qty{1.5}{\giga\electronvolt}.
        The last track added, the one that pushed the sum over this threshold, is chosen.
        The value of \qty{1.5}{\giga\electronvolt} comes from the mass of the c quark.
    \item \label{num trks per jet} \textbf{Number of selected tracks:} This is the number of tracks in the jet.
    \item \label{jet pt} \textbf{Jet \(p_\text{T}\)} 
    \item \label{jet eta} \textbf{Jet \(\eta\)} 
    \item \label{parton flavour} The target variable \textbf{parton flavour}. Partons generated by the simulation that are within \(\Delta R <0.3\) of the jet axis may give the jet flavour. If there is more than one candidate then the order of preference is b, c then light partons (`udsg'). \cite[p. 3]{collaboration_identification_2013}
    \item \label{2D IP sig} The discriminating variable \textbf{track 2D IP significance:} This variable is defined per track. It is used to sort the tracks and in deciding the jet category. 
\end{enumerate}
% These variables will be referred to by number, in the case of variable \ref{3D IP sig} the four values will be referred to as \ref{3D IP sig}a., \ref{3D IP sig}b., \ref{3D IP sig}c. and \ref{3D IP sig}d.

A table showing which variables are available in each jet category is shown in figure \ref{fig:jetClassTable}. 
\begin{figure}
    \begin{tabular}{r|c|c|c|}
        Variable Name& RecoVertex & PsudoVertex & NoVertex \\
        \hline
        \textbf{SV 2D flight distance significance}&\ref{SV 2D flight dist sig}&--&--\\
        \textbf{Number of SVs}&\ref{num SVs}&--&--\\
        \textbf{Track \(\eta_\text{rel}\)}&\ref{trk eta rel}&\ref{trk eta rel}&\ref{trk eta rel}\\
        \textbf{Corrected SV mass}&\ref{SV mass}&\ref{SV mass}&--\\
        \textbf{Number of tracks from the SV}&\ref{num trks from SV}&\ref{num trks from SV}&--\\
        \textbf{SV energy ratio}&\ref{SV energy ratio}&--&--\\
        \textbf{\(\Delta R(\text{SV}, \text{jet})\)}&\ref{SV delta R}&\ref{SV delta R}&--\\
        \textbf{Track 3D IP significance}&\ref{3D IP sig}&\ref{3D IP sig}&\ref{3D IP sig}\\
        \textbf{Track \(p_{\text{T, rel}}\)}&\ref{trk relative pt}&\ref{trk relative pt}&\ref{trk relative pt}\\
        \textbf{\(\Delta R(\text{track}, \text{jet})\)}&\ref{trk delta R}&\ref{trk delta R}&\ref{trk delta R}\\
        \textbf{Track \(p_{\text{T, rel}}\)}&\ref{trk pt ratio}&\ref{trk pt ratio}&\ref{trk pt ratio}\\
        \textbf{Track distance}&\ref{trk distance}&\ref{trk distance}&\ref{trk distance}\\
        \textbf{Track decay length}&\ref{trk decay length}&\ref{trk decay length}&\ref{trk decay length}\\
        \textbf{Summed tracks \(E_\text{T}\) ratio}&\ref{summed trks Et ratio}&\ref{summed trks Et ratio}&\ref{summed trks Et ratio}\\
        \textbf{\(\Delta R(\text{summed tracks}, \text{jet})\)}&\ref{summed delta R}&\ref{summed delta R}&\ref{summed delta R}\\
        \textbf{First track 2D IP significance above c threshold}&\ref{first trk 2D IP sig above charm}&\ref{first trk 2D IP sig above charm}&\ref{first trk 2D IP sig above charm}\\
        \textbf{Number of selected tracks}&\ref{num trks per jet}&\ref{num trks per jet}&\ref{num trks per jet}\\
        \textbf{Jet \(p_\text{T}\)}&\ref{jet pt}&\ref{jet pt}&\ref{jet pt}\\  
        \textbf{Jet \(\eta\)}&\ref{jet eta}&\ref{jet eta}&\ref{jet eta}\\  
    \end{tabular}
    \caption{Table of which variable is present in each class of jet.
    If the variable is present its variable number is printed in the column for that class of jet, otherwise there is a `-'.}
    \label{fig:jetClassTable}
\end{figure}

\subsubsection{Reweighing jet \(\eta\) and jet \(p_\text{T}\)} \label{sec:methreweigh}
There is one more step in the preprocessing of the data.
A reweighed version of the data is produced, in which all jet flavours, b, c, and udsg have a closer distribution in variables \ref{jet pt} (jet \(p_\text{T}\)) and \ref{jet eta} (jet \(\eta\)).

As the mass of a quark and the overall jet momentum are in different energy scales exchanging a b quark for a c quark will not directly impact the jet \(p_\text{T}\).
As such it is undesirable to let an machine learning tool learn to classify based on differences in the distribution of jet \(p_\text{T}\).

There is also no physical reason that exchanging the quark should directly effect jet \(\eta\).

Any differences in the distributions of these variables between the jet flavours may be due to the cuts we have taken or to inaccuracies in the simulation. %TODO is this true?

When the network was trained, batches of 5 jets are selected from the data sample and based on the results of each batch the optimiser improves the weights.
It is done in batches as this improves the training speed.
An epoch has been completed when a number of jets equivalent to the total number in the data set have been trained on.
In forming the batches of 5, the jets are selected at random, but the distribution by which they are selected can be weighted to increase or decrease the chances of certain jets being selected.

As an attempt to prevent the machine learning tools from learning an undesirable feature the jets is reweighed so that the distributions in variables \ref{jet pt} and \ref{jet eta} will match across each target flavour.
This process begins by assigning each jet a weight of \(1\), then iteratively;
\begin{itemize}
    \item A kernal density estimation is used to plot the density of each flavour in both variables according to the jet weights.
    \item An integrated difference between each flavour in these functions is calculated.
    \item If this different is below a threshold the process ends, else continue.
    \item The points of greatest disagreements between the density of the variables being matched are selected.
    \item According to their distance from the selected point the weights are altered to better match the distributions between the flavours.
    \item The process repeats.
\end{itemize}

Plots of this and it's effect on other variables are given in the results, section \ref{sec:reweigh}.
\subsection{CSVv2 imitation}

It is interesting to look at a net close in specifications to that of CSVv2.
This facilitates comparisons with the existing results in \cite{cms_collaboration_identification_2017} and ensures that the results are representative of those for a realistic problem difficulty.
The network is written in python using the library \lstinline{pytorch}. \cite{paszke_automatic_2017}

The network will have one hidden layer, with twice the number of hidden units as the number of inputs used. 
The activation function in the hidden layer is the ReLU function.
The output layer has three targets, b-jet, c-jet and udsg-jet. 
It uses a linear activation function but the loss incorporates a sigmoid, it is done this way because it allows \lstinline{pytorch} to take advantage to the numerical stability of an analytic gradient.


The loss function used was written by Yaozong Gao (UNC-Chapel Hill). \cite{_yaozong_}
Further an adaptive learning rate scheduler written by Jiaming-Lui was used. \cite{liu_pytorchlrscheduler_2018}
Although both of these things have been incorporated into the \lstinline{pytorch} library in more recent versions, the version of \lstinline{pytorch} available in the environment I was training in did not include them.

All code and scripts used for this project can be found in the repository \lstinline{https://bitbucket.org/tidefall/jettagging}.


\subsection{Minimal approach}\label{sec:methmin}
It is interesting to see what can be done with a limited set of variables.
This could indicate which variables are of most use to the classifier, and therefore which variables have most physical significance.
If a variable offered more discriminating power than we had expected it might indicate an effect that has been overlooked.

The variables relating to the secondary vertex and known to have good discriminating power, so it is logical to begin by investigating them.
It also seems possible that combinations of variables that are less correlated will collectivity contain more information.
Correlation implies a relation between the variables, which itself implies some commonality in the information they hold.

The Kendall rank correlation coefficient will be greater than zero for any consistently increasing function and less than zero for any consistently decreasing function. \cite{kendall_new_1938}
The Kendall rank correlations for all the input variables can be seen in figure \ref{fig:allKendall}, the correlations for only the least correlated variables can be seen in figure \ref{fig:smallKendall}.

\begin{figure}
  \centerline{
    \includegraphics[width=1.6\textwidth]{all_kendall.eps}
   }
   \caption{The Kendall correlation coefficients for all of the variables used as inputs. Track wise variables have a number at the end of them name indicating which track of the jet is being used.}
    \label{fig:allKendall}
\end{figure}

\begin{figure}
    \includegraphics[width=\textwidth]{small_kendall.eps}
    \caption{The Kendall correlation coefficients for the variables with the least correlation that will be used as inputs. Track wise variables have a number at the end of them name indicating which track of the jet is being used.}
    \label{fig:smallKendall}
\end{figure}


Using these values, lists of least correlated variables can be constructed. 
The process goes as follows;

\begin{enumerate}
    \item Pick two variables as a start point. This will be the two variables with the lowest correlation that have yet to be used as a start point.
    \item Find the variable that has the lowest combined correlation with all variables already chosen.
        Add this variable to the list of variables.
    \item Continue until all or enough variables have been chosen.
\end{enumerate}

Taking the 6 least correlated variables with the first 3 starting points gives these ordered lists;

\begin{tabular}{lll}
    list 1& list 2& list 3\\
    \hline
    \ref{trk pt ratio} ``trk pt ratio 1"&\ref{trk distance} ``trk distance 1"&\ref{trk decay length} ``trk decay length 1"\\
    \ref{trk eta rel} ``trk eta rel 1"&\ref{3D IP sig} ``3D IP sig 1"&\ref{trk pt ratio} ``trk pt ratio 1"\\
    \ref{SV delta R} ``SV delta R"&\ref{SV delta R} ``SV delta R"&\ref{trk distance} ``trk distance 1"\\
    \ref{trk decay length} ``trk decay length 1"&\ref{trk eta rel} ``trk eta rel 1"&\ref{SV delta R} ``SV delta R"\\
    \ref{trk distance} ``trk distance 1"&\ref{trk pt ratio} ``trk pt ratio 1"&\ref{trk eta rel} ``trk eta rel 1"\\
    \ref{SV 2D flight dist sig} ``SV 2D flight dist sig"&\ref{jet pt} ``jet pt"&\ref{SV 2D flight dist sig} ``SV 2D flight dist sig"\\
\end{tabular}

Of these lists 1 and 3 are simply reorderings, so 1 and 2 will be used. Lists beyond 3 will have a higher correlation and are therefore not considered.

